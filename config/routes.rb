Rails.application.routes.draw do
  resources :thermostats, param: :household_token, only: [] do
    member do
      get :stats
    end
    resources :readings, param: :sequential_thermostat_reading_id, only: [:create, :show]
  end
end
