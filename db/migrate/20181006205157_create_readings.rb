class CreateReadings < ActiveRecord::Migration[5.2]
  def change
    create_table :readings do |t|
      t.belongs_to :thermostat, index: true

      t.integer :sequential_thermostat_reading_id, null: false
      t.float  :temperature
      t.float  :humidty
      t.float  :battery_charge

      t.timestamps
    end
  end
end
