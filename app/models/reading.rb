class Reading < ApplicationRecord
  belongs_to :thermostat
  acts_as_sequenced scope: :thermostat_id, column: :sequential_thermostat_reading_id

  validates :temperature, :humidty, :battery_charge, presence: true
end
