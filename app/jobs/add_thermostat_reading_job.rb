class AddThermostatReadingJob < ApplicationJob
  queue_as :default

  def perform(thermostat_id, reading_params)
    thermostat = Thermostat.find(thermostat_id)
    reading = thermostat.readings.new(JSON.parse(reading_params))
    reading.save
  end
end