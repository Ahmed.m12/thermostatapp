class ReadingSerializer < ActiveModel::Serializer
  attributes :sequential_thermostat_reading_id, :temperature, :humidty, :battery_charge
end
