class ThermostatsController < ApplicationController
  before_action :set_thermostat

  def stats
    readings = @thermostat.readings.pluck(:temperature, :humidty, :battery_charge).transpose

    temperature_readings = readings[0]
    humidty_readings = readings[1]
    battery_charge_readings = readings[2]

    result = {
      temperature_readings_average:    temperature_readings.sum / temperature_readings.count,
      temperature_readings_minimum:    temperature_readings.min,
      temperature_readings_maximum:    temperature_readings.max,
      humidty_readings_average:        humidty_readings.sum / humidty_readings.count,
      humidty_readings_minimum:        humidty_readings.min,
      humidty_readings_maximum:        humidty_readings.max,
      battery_charge_readings_average: battery_charge_readings.sum / battery_charge_readings.count,
      battery_charge_readings_minimum: battery_charge_readings.min,
      battery_charge_readings_maximum: battery_charge_readings.max
    }

    render json: {success: true, stats: result}
  end

  private
    def set_thermostat
      @thermostat = Thermostat.find_by(household_token: params[:household_token])
      unless @thermostat
        render json: {success: false, message: "Unauthorized thermostat"}
      end
    end
end
