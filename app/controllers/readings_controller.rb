class ReadingsController < ApplicationController
  before_action :authenticate_thermostat
  before_action :set_reading, only: [:show]

  def show
    render json: @reading
  end

  def create
    next_thermostat_reading_id = @thermostat.readings.last.present? ? @thermostat.readings.last.sequential_thermostat_reading_id + 1 : 1

    @reading = @thermostat.readings.new(reading_params)
    if @reading.valid?
      AddThermostatReadingJob.perform_later(@thermostat.id, reading_params.to_json)
      render json: {success: true, reading_id: next_thermostat_reading_id.as_json, message: "Thermostat's readings have been added successfully."}
    else
      render json: {success: false, errors: @reading.errors.full_messages}
    end
  end

  private
    def set_reading
      @reading = @thermostat.readings.find_by(sequential_thermostat_reading_id: params[:sequential_thermostat_reading_id])
      unless @reading
        render json: {success: false, message: "Unknown thermostat readings"}
      end
    end

    def reading_params
      params.require(:reading).permit(:temperature, :humidty, :battery_charge)
    end

    def authenticate_thermostat
      @thermostat = Thermostat.find_by(household_token: params[:thermostat_household_token])
      unless @thermostat
        render json: {success: false, message: "Unauthorized thermostat"}
      end
    end
end
